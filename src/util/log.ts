export class Log {
    public static error(err: string, clase: string, method: string) {
        console.log("\x1b[33m%s\x1b[0m", "[Cartagena > " + clase + " > " + method + "] - Error: " + err);
    }

    public static warn(msg: string, clase: string, method: string) {
        console.log("[Cartagena" + (clase ? " > " : "") + clase + ((method) ? " > " : "") + method + "]: " + msg);
    }
}
