import *  as fs from "fs";
import { Log } from "./log";

class Service {
    static class = "FileUtil";

    constructor() {
    }

    public static async saveFile(path: string, data: any) {
        return new Promise(async (resolve, reject) => {
            fs.writeFile(process.env.OUT_PATH + path, Service.convertToCSV(data), 'utf8', function (err) {
                if (err) {
                    Log.error(err.stack, Service.class, 'saveFile');
                    return reject();
                }
                return resolve();
            });
        });
    };

    static convertToCSV(objArray: any) {
        const array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
        const cabeceras = Object.keys(array[0]);
        let str = '\"' + cabeceras.join('"' + process.env.OUT_SEPARATOR + '"') + '\"\r\n';

        for (let i = 0; i < array.length; i++) {
            let line = '';
            for (const index in array[i]) {
                if (line != '') line += process.env.OUT_SEPARATOR;
                line += '"' + array[i][index] + '"';
            }
            str += line + '\r\n';
        }
        return str;
    }

    static async readIntegracionesFile(): Promise<any[]> {
        return new Promise(async (resolve, reject) => {
            fs.readFile(process.env.INTEGRACIONES, (err, data: any) => {
                if (err) {
                    Log.error(err.stack, Service.class, 'readJsonFile');
                    return reject();
                }
                resolve(JSON.parse(data));
            });
        });
    }
}


export let FileUtil = Service;
