import * as mysql from 'mysql';
import { Log } from "./log";

class Service {
    static class = "BDUtil";

    constructor() {
    }

    public static connect(data: any) {
        const connection = mysql.createConnection(data);
        connection.connect();
        return connection;
    }

    public static query(connection: any, sql: string, values: any = []) {
        return new Promise(async (resolve, reject) => {
            connection.query(sql, values, (error: any, results: any, fields: any) => {
                if (error) {
                    Log.error(error, Service.class, 'Query');
                    resolve(error);
                }
                resolve(results);
            });
        });
    }

    public static end(connection: any) {
        connection.destroy();
    }
}


export let BDUtil = Service;