import { BDUtil } from "../util/bd";

class Periodo {
    public getPeriodos(start: number, end: number) {
        const data: any = [];
        for (let y = start; y <= end; y++) {
            for (let m = 0; m <= 12; m++) {
                const p: any = this.getPeriodo({y: y.toString(), m: this.pad(m, 2), w: '00'});
                data.push(p);
            }
        }
        return data;
    }

    public getPeriodo({y = '0000', m = '00', w = '00'}: { y: string, m: string, w: string }) {
        const periodo: any = {};
        periodo.mes = isNaN(parseInt(m)) ? 0 : parseInt(m);
        periodo.ejercicio = parseInt(y);
        periodo.semestre = this.calculateSubPeriodo(periodo.mes, 6);
        periodo.trimestre = this.calculateSubPeriodo(periodo.mes, 3);
        periodo.semana_iso = 0;
        periodo.dia = 0;
        periodo.mes_lineal = periodo.ejercicio * 12 + periodo.mes;
        periodo.semana_lineal = 0;
        periodo.nombre_mes = '';
        periodo.id = `${y}${periodo.semestre}${periodo.trimestre}${this.pad(periodo.mes.toString(), 2)}${w}`;
        return periodo;

    }

    public async saveToDB(con: any, periodo: any) {
        return await this.create(con, periodo);
    }

    private async create(con: any, p: any) {
        const create = 'INSERT INTO `d_periodo` (`id`, `ejercicio`, `semestre`, `trimestre`, `mes`, `semana_iso`, `dia`, `mes_lineal`, `semana_lineal`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE `ejercicio` = `ejercicio`';
        const values = [p.id, p.ejercicio, p.semestre, p.trimestre, p.mes, p.semana_iso, p.dia, p.mes_lineal, p.semana_lineal];
        const data: any = await BDUtil.query(con, create, values);
        return data.affectedRows;
    }

    private calculateSubPeriodo(m: number, p: number) {
        return (m) ? Math.trunc((m - 1) / p) + 1 : 0;
    }

    private pad(num: number, size: number) {
        let s = num + "";
        while (s.length < size) s = "0" + s;
        return s;
    };
}

export let PeriodoClass = new Periodo();
