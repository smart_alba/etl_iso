import { sha256 } from 'js-sha256';
import * as moment from 'moment';

export class Auth {
    class = 'AuthClass';

    public static toDayHash(ctrl: string) {
        const today = moment().format('YYYYMMDD');
        return sha256(`${today}${ctrl}`);
    }
}
