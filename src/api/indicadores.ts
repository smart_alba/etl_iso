import { Log } from "../util/log";
import { Auth } from "./auth";
import { RequestService } from "../util/request";
import { PeriodoClass } from "./periodo";
import { BDUtil } from "../util/bd";

class Indicadores {
    class = 'IndicadoresClass';

    public async getIndicadores(y: number, integracion: any) {
        const hash = Auth.toDayHash(integracion.ctrl);
        const url = this.getUrl(integracion.url, y.toString(), hash);
        try {
            const data: any = await RequestService.GET(url);
            if (!Array.isArray(data)) {
                return [];
            }
            return data.map((d: any) => {
                const periodo = PeriodoClass.getPeriodo({y: d.anyo, m: d.periodo, w: '00'});
                const indicador: any = {};
                indicador['id_identificador'] = d.codigo;
                indicador['id_entidad'] = '01';
                indicador['id_periodo'] = periodo.id;
                indicador['valor'] = d.valor;
                indicador['dividendo'] = this.getFloatValor(d.valor);
                indicador['divisor'] = 1;
                indicador['table'] = integracion.table;
                return indicador;
            });
        } catch (err) {
            Log.error(err, this.class, 'GetIndicadores');
            return []
        }
    }

    public async saveToDB(con: any, indicador: any) {
        return await this.create(con, indicador);
    }

    private async create(con: any, i: any) {
        const create = 'INSERT INTO `' + i.table + '` (`id_indicador`, `id_entidad`, `id_periodo`, `valor`, `dividendo`, `divisor`) VALUES (?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE `valor` = ?, `dividendo` = ?;';
        const values = [i.id_identificador, i.id_entidad, i.id_periodo, i.valor, i.dividendo, i.divisor, i.valor, i.dividendo];
        const data: any = await BDUtil.query(con, create, values);
        return data.affectedRows;
    }

    private getUrl(url: string, year: string, hash: string) {
        return `${url}?anyo=${year}&ctrl=${hash}`
    }

    private getFloatValor(v: string) {
        const float = v.split('.').join('').split(',').join('.');
        return isNaN(parseFloat(float)) ? null : parseFloat(float);
    }
}

export let IndicadoresClass = new Indicadores();
