import * as dotenv from "dotenv";
import { PeriodoClass } from "./api/periodo";
import { BDUtil } from "./util/bd";
import { IndicadoresClass } from "./api/indicadores";
import { Log } from "./util/log";
import { FileUtil } from "./util/file";

dotenv.config({path: ".env"});

const connection = {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    database: process.env.DB_DATABASE,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
};

function getYears(init: number, end: number) {
    const years = [];
    for (let y = init; y <= end; y++) {
        years.push(y);
    }
    return years;
}

async function integrate() {
    const integraciones: any[] = await FileUtil.readIntegracionesFile();
    const currentYear = new Date().getFullYear();
    await getPeriodos(currentYear);
    integraciones.forEach(async (integracion: any) => {
        await getIndicadores(currentYear, integracion);
    });
}

async function getIndicadores(currentYear: number, integracion: any) {
    const con = BDUtil.connect(connection);
    const years = getYears(currentYear - 1, currentYear);
    for (const year of years) {
        Log.warn('#######################', integracion.id, year.toString());
        Log.warn('Iniciando integración', integracion.id, '');
        const indicadores = await IndicadoresClass.getIndicadores(year, integracion);
        Log.warn(`Leidos ${indicadores.length} indicadores.`, integracion.id, year.toString());
        for (const indicador of indicadores) {
            const exist = await IndicadoresClass.saveToDB(con, indicador);
            if (exist) {
                Log.warn(`${indicador.id_identificador} guardado.`, integracion.id, indicador.id_periodo.toString());
            } else {
                Log.error(`${indicador.id_identificador} error.`, integracion.id, indicador.id_periodo.toString());
            }
        }
    }
    BDUtil.end(con);
}

async function getPeriodos(currentYear: number) {
    const periodos = PeriodoClass.getPeriodos(currentYear - 1, currentYear);
    const con = BDUtil.connect(connection);
    for (const periodo of periodos) {
        const exist = await PeriodoClass.saveToDB(con, periodo);
    }
    BDUtil.end(con);
}

integrate();
